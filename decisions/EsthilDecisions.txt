
country_decisions = {
	B54_move_morgurax = {
		major = yes
		potential = {
			tag = B54
			mission_completed = B54_finishing_touches
		}
		provinces_to_highlight = {
			has_province_modifier = esthil_morgurax
		}
		allow = {
			NOT = { has_country_modifier = morgurax_recentely_moved}
			is_at_war = no
		}
		effect = { 
			country_event = { 
				id = flavor_esthil.12 
			}
			add_country_modifier = { 
    			name = morgurax_recentely_moved
    			duration = 365
    			hidden = no
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
}